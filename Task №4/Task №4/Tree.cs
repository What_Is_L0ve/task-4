﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__4
{
	public class Tree
	{
		private TreeElement _first;
		public void AddElement(int value)
		{
			if (_first == null)
			{
				AddFirst(value);
				Console.WriteLine($"Первый элемент {_first.Value}");
				return;
			}
			AddElement(value, _first);
		}
		private void AddFirst(int value)
		{
			_first = new TreeElement
			{
				Value = value,
				Count = 1
			};
		}
		private void AddElement(int value, TreeElement current)
		{
			if (current == null)
				return;

			if (value == current.Value)
			{
				current.Count++;
				return;
			}
			if (value > current.Value)
			{
				if (current.Right != null)
				{
					AddElement(value, current.Right);
				}
				else
				{
					current.Right = new TreeElement
					{
						Value = value,
						Count = 1
					};
					Console.WriteLine($"Правый элемент: {current.Right.Value}");
				}
			}
			else
			{
				if (value < current.Value)
				{
					if (current.Left != null)
					{
						AddElement(value, current.Left);
					}
					else
					{
						current.Left = new TreeElement
						{
							Value = value,
							Count = 1
						};
						Console.WriteLine($"Левый элемент: {current.Left.Value}");
					}
				}
			}
		}
		public bool SearchElement(int value, out int count)		
			=>SearchElement(value, _first, out count);
		private bool SearchElement(int value, TreeElement current, out int count)
		{
			if (value == current.Value)
			{
				Console.Write($"({current.Value}) <- элемент найден");
				count = current.Count;
				return true;
			}
			if (value > current.Value)
			{
				Console.Write($"{current.Value}, ");
				return SearchElement(value, current.Right, out count);
			}
			else if (value < current.Value)
			{
				Console.Write($"{current.Value}, ");
				return SearchElement(value, current.Left, out count);
			}
			count = 0;
			return false;
		}
		public void PrintElement(int value, out int count)
		{
			if(SearchElement(value, out count))			
				Console.WriteLine($", кол-во повторений {count}\n");			
		}			
	}
}

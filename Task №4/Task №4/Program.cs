﻿using System;
using System.Collections;

namespace Task__4
{
	public class Program
	{
		static void Main(string[] args)
		{
			var count = 0;
			var tree = new Tree();
			var arrayElement = new int[]
			{
				5,
				7,
				8,
				32,
				2,
				73,
				81,
				1,
				3,
				15,
				4,
				0,
				18,
				47,
				9,
				1,
				10,
				8,
				10,
				9,
				5,
				6,
				3,
				7
			};
			for (var i = 0; i < arrayElement.Length; i++)
			{
				tree.AddElement(arrayElement[i]);
			}
			for(var i = 0; i < arrayElement.Length; i++)
			{
				tree.PrintElement(arrayElement[i],out count);
			}			
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task__4
{
	public class TreeElement
	{
		public TreeElement Left { get; set; }
		public TreeElement Right { get; set; }
		public int Value { get; set; }
		public int Count { get; set; }
	}
}
